Place the most recent version of the RapidJSON header files (e.g. rapidjson.hpp) here. 

You can get the files from:

https://github.com/Tencent/rapidjson/tree/master/include/rapidjson

RapidJSON is made available using the following custom license:

https://github.com/Tencent/rapidjson/blob/master/license.txt
