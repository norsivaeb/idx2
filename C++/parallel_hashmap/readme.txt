Place the most recent version of the parallel hashmap header files here (e.g., phmap.h).

These source files can be obtained from:

https://github.com/greg7mdp/parallel-hashmap/tree/master/parallel_hashmap

parallel-hashmap is made available under the Apache License 2.0 license